import os

def rename_files():
    # (1) get the file names from a given folder
    file_list = os.listdir(r"F:\Users\Saioa\Proyectos\Udacity\Python\prank")

    os.chdir(r"F:\Users\Saioa\Proyectos\Udacity\Python\prank")
    
    # (2) for each file, rename filename
    for file_name in file_list:
        os.rename(file_name, file_name.translate(None, "0123456789"))

rename_files()
