import turtle

def draw_square(bgcolor, turtleShape, turtleColor, turtleSpeed):
    window = turtle.Screen()
    window.bgcolor(bgcolor)

    brad = turtle.Turtle()
    brad.shape(turtleShape)
    brad.color(turtleColor)
    brad.speed(turtleSpeed)
    
    brad.forward(100)
    brad.right(90)
    brad.forward(100)
    brad.right(90)
    brad.forward(100)
    brad.right(90)
    brad.forward(100)
    brad.right(90)

    window.exitonclick()
draw_square("lightcoral","turtle","green", "slowest")
